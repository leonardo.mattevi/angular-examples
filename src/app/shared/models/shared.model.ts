export enum BaseColorsTypes {
    Danger,
    Success,
    Primary,
    Secondary,
    Info,
    Warning,
}
export enum BaseTextColorsTypes {
    Body,
    White,
    Warning,
    Danger,
    Light,
    Dark,
    Black, 
    Muted
}

export function getBgColorClass(bgColor: BaseColorsTypes, prefix = 'bg') {
    return  `${prefix}-${BaseColorsTypes[bgColor].toLocaleLowerCase()}`;
}
export function getTextColorClass(bgColor: BaseColorsTypes) {
    switch(bgColor) {
        case BaseColorsTypes.Primary: 
        case BaseColorsTypes.Secondary: 
        case BaseColorsTypes.Danger: 
        case BaseColorsTypes.Warning:
        case BaseColorsTypes.Info: 
        case BaseColorsTypes.Success: {
            return 'text-' + BaseTextColorsTypes[BaseTextColorsTypes.White].toLocaleLowerCase();
        }
        default: {
            return 'text-' + BaseTextColorsTypes[BaseTextColorsTypes.Body].toLocaleLowerCase();
        }
        
    }
}