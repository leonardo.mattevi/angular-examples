
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertComponent } from './components/alert/alert.component';
import { BadgeComponent } from './components/badge/badge.component';
import { ButtonComponent } from './components/button/button.component';
import { CardComponent } from './components/card/card.component';
import { CollapseComponent } from './components/collapse/collapse.component';
import { CollapseCardComponent } from './components/collapse/components/collapse-card.component';

@NgModule({
  declarations: [
    AlertComponent,
    BadgeComponent,
    ButtonComponent,
    CardComponent,
    CollapseComponent,
    CollapseCardComponent
  ],
  exports: [
    AlertComponent,
    BadgeComponent,
    ButtonComponent,
    CardComponent,
    CollapseComponent,
    CollapseCardComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }