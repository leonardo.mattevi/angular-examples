import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { BaseColorsTypes } from '../../models/shared.model';

@Component({
  selector: 'dcb-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements AfterViewInit {
  @Input() type: BaseColorsTypes = BaseColorsTypes.Info;
  @ViewChild('headerContent') headerContent!: ElementRef;
  @ViewChild('footerContent') footerContent!: ElementRef;
  hideHeader: boolean = false;
  hideFooter: boolean = false;
  BaseColorsTypes = BaseColorsTypes;

  constructor(private changeref: ChangeDetectorRef) { }

  ngAfterViewInit(): void {
    this.hideHeader =  !this.headerContent?.nativeElement?.innerText;
    this.hideFooter =  !this.footerContent?.nativeElement?.innerText;
    this.changeref.detectChanges();
  }

}
