import { Component, EventEmitter, Input, OnInit, Output, SimpleChange } from '@angular/core';

@Component({
  selector: 'dcb-collapse-card',
  template: `
    <div class="card">
      <div class="card-header" (click)="selectThisCard()">
        <ng-content select="[header]"></ng-content>
      </div>
      <div class="collapse" [class]="{'show': selectedIndex === index}">
        <div class="card-body">
          <ng-content select="[body]"></ng-content>
        </div>
      </div>
    </div>
  `,
  styles: [``]
})
export class CollapseCardComponent implements OnInit {
  @Input() index!: number;
  @Input() selectedIndex: number = -100;

  @Output() indexChangeEvent = new EventEmitter<number>();

  isSelected: boolean =  false;
  
  constructor() { }

  ngOnInit(): void {
  }
  selectThisCard() : void {
    this.indexChangeEvent.emit(this.index);
  }
}
