import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'dcb-collapse',
  template: `
    <div class="accordion-box">
      <dcb-collapse-card 
        *ngFor="let item of accordionCardList;; let i = index"
        [index]="i"
        [selectedIndex]="selectedIndex"
        (indexChangeEvent)="indexChange($event)"
        >        
        <h5 class="mb-0" header>
          <button class="btn w-100 text-start">
              click it to open it
          </button>
        </h5>
        <div class="mb-0" body>
          lorem ipsum bla bla bla...
        </div>
        </dcb-collapse-card>
    </div>
  `,
  styles: [``]
})
export class CollapseComponent implements OnInit {
  @Input() accordionCardList: {
    title: string,
    body: string
  }[] = [{ title: 'asd1', body: 'asd1' },{ title: 'asd2', body: 'asd2' }];
  selectedIndex = -1;
  constructor() { }

  indexChange(value: number): void {
    this.selectedIndex = value;
  }
  ngOnInit(): void { }
}
