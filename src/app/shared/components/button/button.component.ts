import { Component, Input, OnInit } from '@angular/core';
import { BaseColorsTypes, getBgColorClass } from '../../models/shared.model';

@Component({
  selector: 'dcb-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() label : string = '';
  @Input() type: BaseColorsTypes = BaseColorsTypes.Success;
  BaseColorsTypes = BaseColorsTypes;
  getBgColorClass = getBgColorClass;
  constructor() { }

  ngOnInit(): void {
  }

}
