import { Component, Input, OnInit } from '@angular/core';
import { BaseColorsTypes, getBgColorClass } from '../../models/shared.model';

@Component({
  selector: 'dcb-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss']
})
export class BadgeComponent implements OnInit {
  @Input() label: string = '';
  @Input() type: BaseColorsTypes = BaseColorsTypes.Success;
  BaseColorsTypes = BaseColorsTypes;
  getBgColorClass = getBgColorClass;
  constructor() { }

  ngOnInit(): void {
  }

}
