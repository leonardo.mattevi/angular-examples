import { Component, Input, OnInit } from '@angular/core';
import { BaseColorsTypes, getBgColorClass, getTextColorClass } from '../../models/shared.model';

@Component({
  selector: 'dcb-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  @Input() message: string = '';
  @Input() type: BaseColorsTypes = BaseColorsTypes.Danger;
  BaseColorsTypes = BaseColorsTypes;
  getBgColorClass = getBgColorClass;
  getTextColorClass = getTextColorClass;

  constructor() { }

  ngOnInit(): void {
  }

}
