import { Component, OnInit } from '@angular/core';
import { BaseColorsTypes } from 'src/app/shared/models/shared.model';

@Component({
  selector: 'dcb-components',
  templateUrl: './components.component.html',
  styles: [
  ]
})
export class ComponentsComponent implements OnInit {
  message = 'old Message...';
  BaseColorsTypes= BaseColorsTypes;
  selectAlertType = BaseColorsTypes.Info;

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {      
      this.message = 'new Message!';
      this.selectAlertType = BaseColorsTypes.Danger;
    }, 5500);
  }

}
